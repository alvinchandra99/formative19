function input(value) {
  let numberBox = document.getElementById("numberBox").value;

  if (value == "+/-") {
    return (document.getElementById("numberBox").value *= "-1");
  }

  if (numberBox == "0" && typeof value == "number") {
    return (document.getElementById("numberBox").value = value);
  }

  if (numberBox == "" && typeof value != "number") {
    return (document.getElementById("numberBox").value = "");
  }
  document.getElementById("numberBox").value += value;
}

function result() {
  let numberBoxVal = document.getElementById("numberBox").value;
  let result = eval(numberBoxVal);
  document.getElementById("numberBox").value = result;
}

function clr() {
  document.getElementById("numberBox").value = "";
}
